//Выведите в разметку перечень
// стран с валютами которых
// работает НБУ (если несколько
// стран имеют общую валюту -
// выводите их все, пример: зона
// Евро, или страны использующие
// USD).

let url = 'https://restcountries.eu/rest/v2/all';
let url1 = 'https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json';
(async function(){
let result=await fetch(url);
result=await result.json();
let result1=await fetch(url1);
result1=await result1.json();

let result2=result.map(el=>{
  return {
      'name':el.name,
      'code':el.currencies[0].code,
      'flag':el.flag,
  };
});

let result3=result1.map(el=>{
    return{
        'cur_name':el.txt,
        'rate':el.rate,
        'cc':el.cc,
        'date':el.exchangedate,
    }
});
    let result5 = result2.map(e => {
        let x = result3.filter(el => (e.code === el.cc));
        x = x.length ? x[0] : {};
        return {
            ...e,
            ...x,
        }
    });
   let result4=result5.filter(el=>el.cur_name);
   console.log(result4);
       let el = document.querySelector('.my_container');

       function createRow(fullName,flagLink) {
           let img=document.createElement('img');
           img.className='img_class';
           img.src=flagLink;

           let name = document.createElement('div');
           name.className = 'my_class';
           name.innerHTML = fullName;

           let row = document.createElement('div');
           row.className = 'border border-primary mt-2';
           row.appendChild(img);
           row.appendChild(name);
           return row;
       };


       result4.map(e => {
           let fullName = `${e.name} (${e.code}-${e.cur_name}) Курс: ${e.rate} на ${e.date}`;
           el.appendChild(createRow(fullName, e.flag));
       });

})();
